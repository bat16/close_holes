@ECHO OFF
setlocal enabledelayedexpansion
ECHO Batch script for filter LAS file by order reflections
ECHO Terrain model: Close Holes
ECHO Author: Marek Szczepkowski
ECHO Date: 08.11.2017
ECHO Company: Visimind Ltd
ECHO Version: 1.3
ECHO For SAGA 6.0, LASTool , QGIS 2.14
ECHO.

SET SAGA_ROOT="D:\Programy\release_1.0.0\saga-6.0.0_x64\"
SET QGIS_ROOT=C:\Program Files\QGIS 3.20.0

REM Setup gdal_transalte.exe
SET PATH=%PATH%;%QGIS_ROOT%\bin
SET GDAL_DATA=%QGIS_ROOT%\share\gdal

REM Setup saga_cmd.exe
SET PATH=%PATH%;%SAGA_ROOT%

REM Path to working dir
SET WORK=%cd%

REM Suffix output files
SET OUT_SUFFIX=_CH

REM Cunter 
dir /b *.las 2> nul | find "" /v /c > tmp && set /p countLAS=<tmp && del tmp

REM Create DIR
mkdir Close_Holes



REM Engine
set /A Counter=1
FOR /F %%i IN ('dir /b "%WORK%\*.las"') DO (
	ECHO. 
	ECHO.
	ECHO Processing %%i....    !Counter! / %countLAS% FILES
	ECHO            CloseHoles %%i	

	REM CloseHoles	
    saga_cmd --flags=r io_shapes_las "Import LAS Files" -FILES:"%WORK%\%%i" -POINTS:"%WORK%\%%i" -RGB_RANGE:0
    saga_cmd --flags=r pointcloud_tools "Point Cloud to Grid" -POINTS="%WORK%\%%~ni.sg-pts" -GRID="%WORK%\%%~ni.sgrd" -OUTPUT:0 -AGGREGATION:3 -CELLSIZE:0.500000
    saga_cmd --flags=r grid_tools "Close Gaps" -INPUT:"%WORK%\%%~ni.sgrd" -RESULT:"%WORK%\%%~ni_closed_gaps.sgrd" -THRESHOLD:0.100000
    
    REM Output ASC
    REM Output TIFF
    saga_cmd --flags=r io_grid "Export ESRI Arc/Info Grid" -GRID:"%WORK%\%%~ni_closed_gaps.sgrd" -FILE:"%WORK%\Close_Holes\%%~ni%OUT_SUFFIX%.asc" -FORMAT:1 -GEOREF:0 -PREC:4 -DECSEP:0	
	gdal_translate -of GTiff -ot Float32 -co "TFW=YES" "%WORK%\Close_Holes\%%~ni%OUT_SUFFIX%.asc" "%WORK%\Close_Holes\%%~ni%OUT_SUFFIX%.tif"
   
    REM clear
	DEL /Q "%WORK%\*.sg-pts"
    DEL /Q "%WORK%\*.sg-info"
	DEL /Q "%WORK%\*.mgrd"
	DEL /Q "%WORK%\Close_Holes\*.prj"	
    DEL /Q "%WORK%\*.prj"		
	DEL /Q "%WORK%\*.sdat"	
	DEL /Q "%WORK%\*.sgrd"	
	
	set /A Counter+=1	
)
ECHO SCRIPT ENDS WITHOUT ERRORS
PAUSE